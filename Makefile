C = latexmk

master = arccss14_git.pdf
auxdocs = lstparams.tex baparams.tex
auxdocs += introduction.tex standalone.tex distributed.tex
auxdocs += git_svn.tex conclusion.tex references.tex
auxdocs += git_standalone.txt git_distributed.txt git_svn.txt git_rollback.txt
auxdocs += pointy.tex thanks.tex

.SUFFIXES: 
.SUFFIXES: .pdf .tex .fig .pdf_t

FIGURES=figure_standalone.pdf figure_distributed.pdf figure_git_svn.pdf figure_logo.pdf
TFIGURES=${FIGURES:.pdf=.pdf_t}

${master} : ${master:.pdf=.tex} ${auxdocs} ${TFIGURES}
	$(C) -pdf -pdflatex="pdflatex -interactive=nonstopmode" -use-make $<

default: $(master)

clean:
	latexmk -CA
	rm -rf ${master} *.pdf_t

.fig.pdf_t :
	fig2dev -L pdftex -D -500 $< ${subst .pdf_t,.pdf,$@} 
	fig2dev -L pdftex_t -D -500 -p ${subst .pdf_t,.pdf,$@} $< $@

figure_distributed.pdf_t : Bitbucket_Logo.gif
figure_git_svn.pdf_t : Subversion.gif

${master} : arccss14_git.tex $(FIGURES)


.PHONY: default clean
